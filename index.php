
<?php 

require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");


$sheep = new Animal("shaun");

echo "Nama Hewan : $sheep->name <br> "; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 2
echo "Hewan Beerdarah Dingin : $sheep->cold_blooded <br><br>"; // false

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : $sungokong->name <br> "; // "shaun"
echo "Jumlah Kaki : $sungokong->legs <br>"; // 2
echo "Hewan Beerdarah Dingin : $sungokong->cold_blooded <br>";
echo $sungokong->yell() . "<br><br>";// "Auooo"

$kodok = new Frog("buduk");
echo "Nama Hewan : $kodok->name <br> "; // "shaun"
echo "Jumlah Kaki : $kodok->legs <br>"; // 2
echo "Hewan Beerdarah Dingin : $kodok->cold_blooded <br>";
echo $kodok->jump()."<br>" ; // "hop hop"


 ?>
